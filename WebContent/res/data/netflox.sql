-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Ven 10 Janvier 2014 à 04:19
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `netflox`
--
CREATE DATABASE IF NOT EXISTS `netflox` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `netflox`;

-- --------------------------------------------------------

--
-- Structure de la table `bon`
--

CREATE TABLE IF NOT EXISTS `bon` (
  `id` int(11) NOT NULL,
  `idFilm` varchar(9) CHARACTER SET utf8 DEFAULT NULL,
  `dateU` bigint(20) DEFAULT NULL,
  `type` enum('A','L') CHARACTER SET utf8 DEFAULT NULL,
  `idClientU` int(11) DEFAULT NULL,
  `utilise` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(10) CHARACTER SET utf8 NOT NULL,
  `nom` varchar(10) CHARACTER SET utf8 NOT NULL,
  `prenom` varchar(10) CHARACTER SET utf8 NOT NULL,
  `solde` int(11) NOT NULL,
  `psswd` varchar(10) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pseudo` (`pseudo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `pseudo`, `nom`, `prenom`, `solde`, `psswd`) VALUES
(1, 'admin', 'boss', 'chef', 0, 'nimda'),
(2, 'toto', 'toto', 'titi', 50, 'azerty');

-- --------------------------------------------------------

--
-- Structure de la table `vente`
--

CREATE TABLE IF NOT EXISTS `vente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idFilm` varchar(9) CHARACTER SET utf8 DEFAULT NULL,
  `date` bigint(15) NOT NULL,
  `prix` int(11) NOT NULL,
  `idClient` int(11) NOT NULL,
  `type` enum('A','L','B') CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
