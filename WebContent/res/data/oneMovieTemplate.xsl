<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output indent="yes" method="xml"/>
  <xsl:template match="/">
    <fo:root>
      <fo:layout-master-set>
        <fo:simple-page-master margin="2cm" master-name="A4-portrait" page-height="29.7cm" page-width="21.0cm">
          <fo:region-body/>
        </fo:simple-page-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="A4-portrait">
        <fo:flow flow-name="xsl-region-body">
            
            <fo:block font-size="14pt" font-weight="bold">
                Détails du film :
            </fo:block>
                        
            <fo:block>
                <xsl:apply-templates select="//movie[title='The Shawshank Redemption']"/>          
            </fo:block>
            
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
  
  <!-- for each movie -->
  <xsl:template match="movie">  
  <!-- title and alternate title -->
    <fo:block font-size="14pt" font-weight="bold"> Titre : <fo:inline font-weight="normal"><xsl:value-of select="title"/></fo:inline>
        <fo:block/>
            <xsl:for-each select="alternate_titles/atitle">	
            	<xsl:sort select="."/>
	            	<fo:block font-size="12pt" font-weight="normal">	                	
	                	<fo:inline font-style="italic" font-weight="normal">
		                	<xsl:choose>						 
							  <xsl:when test="./@country = 'de'">
							   * Altérnatif Allemand :
							  </xsl:when>
							  <xsl:when test="./@country = 'fr'">
							   * Altérnatif Français :
							  </xsl:when>
							  <xsl:when test="./@country = 'it'">
							   * Altérnatif Italien :
							  </xsl:when>
							  <xsl:otherwise>
							   * Inconnu!
							  </xsl:otherwise>
							</xsl:choose>
						</fo:inline>
						<xsl:text> </xsl:text> 
						<xsl:value-of select="."/>	                		                	
	                </fo:block>
            </xsl:for-each>      
        
    </fo:block>
     <!-- deractors -->
    <fo:block font-size="14pt" font-weight="bold"> 
    	
    		 <xsl:choose>
			    <xsl:when test="count(directors/*) &lt;= 1">
			      Réalisateur :
			    </xsl:when>
			    <xsl:otherwise>
			      Réalisateurs :
			    </xsl:otherwise>
  			</xsl:choose>
        <fo:block/>	
            <xsl:for-each select="directors/director">	
            	<xsl:sort select="name/lastname"/>
	            	<fo:block font-size="12pt" font-weight="normal">	
	            	    *                	
	                	<xsl:value-of select="name/firstname"/> 
						<xsl:text>  </xsl:text> 
						<xsl:value-of select="name/lastname"/>                		                	
	                </fo:block>
            </xsl:for-each>      
        
    </fo:block>
    
     <!-- actors -->
    <fo:block font-size="14pt" font-weight="bold"> Acteurs : 
        <fo:block/>	
            <xsl:for-each select="actor">	
            	<xsl:sort select="name/lastname"/>
	            	<fo:block font-size="12pt" font-weight="normal">   
	            	    *
	            	    <fo:inline font-style="italic" font-weight="bold">
	                	<xsl:value-of select="name/firstname"/> 
						<xsl:text>  </xsl:text> 
						<xsl:value-of select="name/lastname"/>
						</fo:inline>
						<xsl:text>     joue le rôle de : </xsl:text>   
						<fo:inline font-style="italic" font-weight="bold">
		               		 <xsl:value-of select="role"/>   
						</fo:inline>           		                	
	                </fo:block>
            </xsl:for-each>      
        
    </fo:block>
    <fo:block font-size="14pt" font-weight="bold"> Année de sortie : <fo:inline font-size="12pt" font-weight="normal"><xsl:value-of select="year"/></fo:inline></fo:block>
    <fo:block font-size="14pt" font-weight="bold"> Durée en minutes : <fo:inline font-size="12pt" font-weight="normal"><xsl:value-of select="runtime"/></fo:inline></fo:block>
    <fo:block font-size="14pt" font-weight="bold"> Résumé :</fo:block><fo:block/>	
    <fo:block font-size="12pt" text-align="justify"> <xsl:value-of select="summary"/></fo:block>
    
    
  </xsl:template>


</xsl:stylesheet>