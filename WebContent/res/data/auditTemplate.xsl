<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output indent="yes" method="xml"/>
  <xsl:template match="/">
    <fo:root>
      <fo:layout-master-set>
        <fo:simple-page-master margin="2cm" master-name="A4-portrait" page-height="29.7cm" page-width="21.0cm">
          <fo:region-body/>
        </fo:simple-page-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="A4-portrait">
        <fo:flow flow-name="xsl-region-body">
            
            <fo:block font-size="16pt" font-weight="bold">
                Audit des ventes location et bon :
                
            </fo:block>
            
            <fo:block>
                <xsl:apply-templates select="//table"/>                     
            </fo:block>
                            
            <fo:block>
               
            </fo:block>
            
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
  
  <xsl:template match="table">  
    <fo:block>
     Audit des <xsl:value-of select="./@id"/> 
    </fo:block>
    <!-- we create a table -->
    <fo:table border-style="solid"  >            
    <fo:table-column column-width="100mm" border-style="solid" />
    <fo:table-column column-width="30mm" border-style="solid" />
    
    <fo:table-header>
        <fo:table-row border-style="solid" >
            <fo:table-cell padding-left = "2mm" padding-top = "2mm" padding-bottom = "2mm">
                <fo:block font-weight="bold">Films</fo:block>
            </fo:table-cell>
            <fo:table-cell  padding-left = "2mm" padding-top = "2mm" padding-bottom = "2mm">
                <fo:block font-weight="bold">Occurrences</fo:block>
            </fo:table-cell >
        </fo:table-row>
    </fo:table-header>
       
    <fo:table-body>
    
    <xsl:for-each select="film" >	
        <xsl:sort select="./name" />	
        
        <fo:table-row border-style="solid" >
            <fo:table-cell padding-left = "2mm" padding-top = "2mm" padding-bottom = "2mm">
                <fo:block font-weight="bold">
                    <xsl:value-of select="name"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell  padding-left = "2mm" padding-top = "2mm" padding-bottom = "2mm">
                <fo:block font-weight="bold">
                    <xsl:value-of select="nombre"/>
                </fo:block>
            </fo:table-cell >
        </fo:table-row>
    	     
    </xsl:for-each>
      
                
    </fo:table-body>   
    </fo:table>
    
  </xsl:template>

</xsl:stylesheet>