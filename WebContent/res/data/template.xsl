<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/">
    <fo:root>
      <fo:layout-master-set>
        <fo:simple-page-master master-name="A4-portrait"
              page-height="29.7cm" page-width="21.0cm" margin="2cm">
          <fo:region-body/>
        </fo:simple-page-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="A4-portrait">
        <fo:flow flow-name="xsl-region-body">
            
            <fo:block font-size="14pt">
                Liste de tous les films :
                <fo:inline/>
            </fo:block>
            
            <fo:block font-size="12pt">
            <!-- all movies in a table-->
         
            <fo:table border-style="solid"  >            
            <fo:table-column column-width="130mm" border-style="solid" />
            <fo:table-column column-width="18mm" border-style="solid" />
            <fo:table-column column-width="18mm" border-style="solid" />
            
            <fo:table-header>
              <fo:table-row border-style="solid" >
                <fo:table-cell padding-left = "2mm" padding-top = "2mm" padding-bottom = "2mm">
                  <fo:block font-weight="bold">Titre</fo:block>
                </fo:table-cell>
                <fo:table-cell  padding-left = "2mm" padding-top = "2mm" padding-bottom = "2mm">
                  <fo:block font-weight="bold">Année</fo:block>
                </fo:table-cell >
                <fo:table-cell  padding-left = "2mm" padding-top = "2mm" padding-bottom = "2mm">
                  <fo:block font-weight="bold">Durée (Min)</fo:block>
                </fo:table-cell >
              </fo:table-row>
            </fo:table-header>
            
            <fo:table-body>
                <xsl:apply-templates select="//movie" >
                    <xsl:sort select="title" />
                </xsl:apply-templates>
            </fo:table-body>   
            </fo:table>
          
             <!-- all movies in a table END -->
          </fo:block >
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
  
  <xsl:template match="movie">  

   <fo:table-row>
    <fo:table-cell  padding-left = "2mm" padding-top = "2mm" padding-bottom = "2mm">
      <fo:block><xsl:value-of select="title"/></fo:block>
    </fo:table-cell>
    <fo:table-cell  padding-left = "2mm" padding-top = "2mm" padding-bottom = "2mm">
      <fo:block><xsl:value-of select="year"/></fo:block>
    </fo:table-cell>
     <fo:table-cell  padding-left = "2mm" padding-top = "2mm" padding-bottom = "2mm">
      <fo:block><xsl:value-of select="runtime"/></fo:block>
    </fo:table-cell>
  </fo:table-row>
  <xsl:apply-templates/>
</xsl:template>


</xsl:stylesheet>