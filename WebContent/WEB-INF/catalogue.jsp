<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="styles/menuStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/catalogueStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/bodyContentStyle.css" />
<title>Films</title>
</head>
<body>
	<div class="bodyContent">
<!-- Tester si l'util est un client un administrateur ou pas pour controller les acces -->
    <c:choose>
		<c:when test="${sessionScope.sessionUtilisateur.id ==  1 }">
			<c:import url="../menus/menuAdmin.jsp"></c:import>
		</c:when>
		<c:otherwise >
			<c:import url="../menus/menuClient.jsp"></c:import>
		</c:otherwise>
	</c:choose>
	<div class="content">
		<!-- recherche -->
		<form method="POST" action="recherche">
			<input name="recherche" type="text" id="recherche" /> <select
				name="Categorie" size=1>
				<option value="Titre">Titre</option>
				<option value="Acteur">Acteur</option>
				<option value="Description">Description</option>
			</select> <input value="Rechercher" tabindex="4" type="submit" id="u_01_d"></input>
		</form>

		<div id="tabTitle">
			<h2>
				Titres des films
				<c:out value="${ordre}" />
			</h2>
		</div>
		<div id="CtalogueId">
			<table id="moviesShow" >
				<tr>
			<!-- Afficher les informations de chaque film -->	
					<th style="border: 1px solid black;"><a href="catalogue">Titre</a>
					</th>
					<th style="border: 1px solid black;"><a
						href="catalogueDateTri">Ann&eacutee</a></th>
					<th>Acteurs</th>
				</tr>
				<c:forEach var="film" items="${FilmsList}">
					<tr>
						<td style="border: 1px solid black;"><a
							href="ficheFilm?titre=${film.titre}">${film.titre}</a></td>
						<td style="border: 1px solid black;">${film.annee}</td>
						<td style="border: 1px solid black;">
						<c:set var= "listActeur" value= "" />
						<c:forEach var="act" items="${film.tabActeur}">
						<c:set var= "listActeur" value= "${listActeur}${act.prenom} ${act.nom} ," />
						</c:forEach>
						<c:set var= "listActeurF" value= "${fn:substring(listActeur, 0,fn:length(listActeur)-1)}" />
						<c:out value = "${listActeurF}" />
						   </td>
					</tr>

				</c:forEach>

			</table>
			<c:set var="parDate" value="dans l'ordre chronologique"/> 
			<c:set var="parTitre" value="dans l'ordre alphabetique"/> 
			
			<c:if test="${ordre  ==   parDate }">
				<c:set var="lien1" value="catalogueDateTri" />
			</c:if>
			<c:if test="${ordre  == parTitre }">
				<c:set var="lien1" value="catalogue" />
			</c:if>
			<%--Afficher la page precedente --%>
			<c:if test="${currentPage != 1}">
				<c:set var="lien"
					value='${lien1}?page=${currentPage - 1}' />
				<td><a href=${lien}>Previous</a></td>
			</c:if>

			<%--Afficher la table des pages  --%>
			<table id="tableNumber" border="1">
				<tr>
					<c:forEach begin="1" end="${noOfPages}" var="i">
						<c:choose>
							<c:when test="${currentPage eq i}">
								<td>${i}</td>
							</c:when>
							<c:otherwise>
								<c:set var="lien"
									value='${lien1}?page=${i}' />
								<td><a href="${lien}">${i}</a></td>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</tr>
			</table>

			<%--Afficher la page suivant --%>
			<c:if test="${currentPage lt noOfPages}">
				<c:set var="lien"
					value='${lien1}?page=${currentPage + 1}' />
				<td><a href="${lien}">Next</a></td>
			</c:if>
		</div>

		<h4>
			<a href="<c:url value="catalogue.pdf"/>"> Générer un PDF du
				catalogue. </a>
		</h4>
	</div>
	</div>
</body>
</html>
