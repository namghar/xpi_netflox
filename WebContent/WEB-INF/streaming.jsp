<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="styles/menuStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/connexionStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/bodyContentStyle.css" />
<link type="text/css" rel="stylesheet"
	href="styles/border.css" />
<title>Streaming</title>
</head>
<body>
	<div class="bodyContent">
	<c:import url="../menus/menuClient.jsp"></c:import>	
	<div class="content">
	<fieldset>
		<legend>Streaming</legend>
		<center>
			<h1>${titre}</h1>
			<br/>
			
			<video width="800" height="500" controls>
  				  <source src="/res/video/KFPanda.mp4" type="video/x-msvideo">				 
				 <object data="/res/video/KFPanda.mp4" type="video/x-msvideo" width="360" height="326">
  					  <param name="filename" value="/res/video/KFPanda.mp4" />  
					  <param name="autostart" value="true" />  
					  <param name="loop" value="true" />  
				</object>
			</video>
		
		</center>
	</fieldset>
	</div>
	</div>
</body>
</html>