<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Inscription</title>
        <link type="text/css" rel="stylesheet" href="styles/menuStyle.css" />	
        <link type="text/css" rel="stylesheet" href="styles/connexionStyle.css" />
    </head>
    <body>
        <c:import url="../menus/menu.jsp"></c:import>   
        <form method="post" action="connexion">
            <fieldset>
                <legend>Connection</legend>
                <p>Vous pouvez vous connecter via ce formulaire.</p>

                <label for="pseudo">Pseudo <span class="requis">*</span></label>
                <input type="text" id="pseudo" name="pseudo" value="<c:out value="${utilisateur.pseudo}"/>" size="20" maxlength="60" />
                <span class="erreur">${form.erreurs['pseudo']}</span>
                <span class="erreur">${form.erreurs['userexist']}</span>
                <br />

                <label for="motdepasse">Mot de passe <span class="requis">*</span></label>
                <input type="password" id="motdepasse" name="motdepasse" value="" size="20" maxlength="20" />
                <span class="erreur">${form.erreurs['motdepasse']}</span>
                <br />               

                <input type="submit" value="Connexion" class="sansLabel" />
                <br />                         
               
                <p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
                
                <%-- Vérification de la présence d'un objet utilisateur en session --%>
                <c:if test="${!empty sessionScope.sessionUtilisateur}">         
                	<c:redirect url="FilmsSelect"/>
                </c:if>              
            </fieldset>
        </form>
    </body>
</html>