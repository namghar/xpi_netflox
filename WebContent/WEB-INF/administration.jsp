<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="styles/menuStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/bodyContentStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/catalogueStyle.css" />
<title>Gestion compte client</title>
</head>
<body>
	<div class="bodyContent">
	<c:import url="../menus/menuAdmin.jsp"></c:import>
		<div class="contentWoCenter">
		<h4>
			<a href="<c:url value="catalogue"/>"> Catalogue </a>
		</h4>
		<h4>
			<a href="<c:url value="ajoutFilm"/>"> Gestion Catalogue </a>
		</h4>
		<h4>
			<a href="<c:url value="gestioncomptes"/>"> Gestion Clients </a>
		</h4>
		<h4>
			<a href="<c:url value="audit.pdf"/>"> Générer un Audit ! </a>
		</h4>
		</div>
	</div>
</body>
</html>