<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="styles/menuStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/connexionStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/bodyContentStyle.css" />
<link type="text/css" rel="stylesheet"
	href="styles/border.css" />
<title>Bon prépayé</title>
</head>
<body>
	<div class="bodyContent">
	<c:import url="../menus/menuClient.jsp"></c:import>
	<div class="content">
	<c:if test="${SoldeI=='1'}">
		<fieldset>
			<legend>Commande refusée</legend>
			<h2>Solde insuffisant</h2>
			<br /> <a href="gestionCompteClient">Recharger votre compte ici</a>
		</fieldset>
	</c:if>
	<form class="Bon" method="POST" action="bon">
		<fieldset>
			<legend>Bon prépayé</legend>
			<p>Sélectionnez le montant que vous souhaitez:</p><br/>
			<select name="montant" size=1>
				<option value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option value="40">40</option>
				<option value="50">50</option>
			</select>
			<br/>
			<input value="Acheter Bon" tabindex="5" type="submit" id="bon"></input>
    	</fieldset>
    	<c:if test="${codeBon!='0'}">
	    	<fieldset>
				<legend>Commande acceptée</legend>
				<p>Voici le code de votre bon :</p><br/>
				<br/>
				<h1>${codeBon}</h1>
	    	</fieldset>
	    </c:if>
	</form>
	</div>
	</div>
</body>
</html>