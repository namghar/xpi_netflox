<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="styles/menuStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/bodyContentStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/connexionStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/inscriptionStyle.css" />
<title>Acceuil</title>
</head>
<body>
	<div class="bodyContent">
	<c:import url="../menus/noMenu.jsp"></c:import>
	
	<!--  login form -->
	
	 <form class = "login" method="post" action="connexion">
            <fieldset>
                <legend>Connection</legend>
                <p>Vous pouvez vous connecter via ce formulaire.</p>

                <label for="pseudo">Pseudo <span class="requis">*</span></label>
                <input type="text" id="pseudo" name="pseudo" value="<c:out value="${utilisateur.pseudo}"/>" size="20" maxlength="60" />
                <span class="erreur">${form.erreurs['pseudo']}</span>
                <span class="erreur">${form.erreurs['userexist']}</span>
                <br />

                <label for="motdepasse">Mot de passe <span class="requis">*</span></label>
                <input type="password" id="motdepasse" name="motdepasse" value="" size="20" maxlength="20" />
                <span class="erreur">${form.erreurs['motdepasse']}</span>
                <br />               

                <input type="submit" value="Connexion" class="sansLabel" />
                <br />                         
               
                <p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
                
                <%-- Vérification de la présence d'un objet utilisateur en session --%>
                <c:if test="${!empty sessionScope.sessionUtilisateur}">         
                	<c:choose>
               		
     					<c:when test="${sessionScope.sessionUtilisateur.pseudo=='admin'}">
      						<c:redirect url="administration"/>	
      					</c:when>

      					<c:otherwise>
     	 					<c:redirect url="catalogue"/>	
     					</c:otherwise>
     					       
					</c:choose>           	
                </c:if>              
            </fieldset>
        </form>
        
    <!-- inscription form -->   
    
    <form class = "subscribe" method="post" action="inscription">
            <fieldset>
                <legend>Inscription</legend>
                <p>Vous pouvez vous inscrire via ce formulaire.</p>

                <label for="pseudo">Pseudo <span class="requis">*</span></label>
                <input type="text" id="pseudo" name="pseudo" value="<c:out value="${utilisateur.pseudo}"/>" size="20" maxlength="60" />
                <span class="erreur">${form.erreurs['pseudo']}</span>
                <span class="erreur">${form.erreurs['userexist']}</span>
                <br />

                <label for="motdepasse">Mot de passe <span class="requis">*</span></label>
                <input type="password" id="motdepasse" name="motdepasse" value="" size="20" maxlength="20" />
                <span class="erreur">${form.erreurs['motdepasse']}</span>
                <br />

                <label for="confirmation">Confirmation du mot de passe <span class="requis">*</span></label>
                <input type="password" id="confirmation" name="confirmation" value="" size="20" maxlength="20" />
                <span class="erreur">${form.erreurs['confirmation']}</span>
                <br />

                <label for="nom">Nom utilisateur</label>
                <input type="text" id="nom" name="nom" value="<c:out value="${utilisateur.nom}"/>" size="20" maxlength="20" />
                <span class="erreur">${form.erreurs['nom']}</span>
                <br />
                
                <label for="prenom">Prenom utilisateur</label>
                <input type="text" id="prenom" name="prenom" value="<c:out value="${utilisateur.prenom}"/>" size="20" maxlength="20" />
                <span class="erreur">${form.erreurs['prenom']}</span>
                <br />

                <input type="submit" value="Inscription" class="sansLabel" />
                <br />
                
                <p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
                 
                <!-- cas de connextion -->
                <c:if test="${!empty sessionScope.sessionUtilisateur}">  
                	
               		<c:choose>
               		
     					<c:when test="${sessionScope.sessionUtilisateur.pseudo=='admin'}">
      						<c:redirect url="administration"/>	
      					</c:when>

      					<c:otherwise>
     	 					<c:redirect url="catalogue"/>	
     					</c:otherwise>
     					       
					</c:choose>           		
                </c:if>        
                
            </fieldset>
        </form>        
	 	</div>	
</body>
</html>