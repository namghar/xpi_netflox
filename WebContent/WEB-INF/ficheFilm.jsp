<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" rel="stylesheet" href="styles/menuStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/catalogueStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/bodyContentStyle.css" />
<title>Fiche du film</title>

</head>
<body>
<div class="bodyContent">
<!-- Tester si l'util est un client un administrateur ou pas pour controller les acces -->
	<c:if test="${sessionScope.nameCl ==  'admin' }">
		<c:import url="../menus/menuAdmin.jsp"></c:import>
	</c:if>
	<c:if test="${sessionScope.nameCl !=  'admin'}">
		<c:import url="../menus/menuClient.jsp"></c:import>
	</c:if>
	<div class="contentWoCenter">
		<c:if test="${SoldeI=='1'}">
			<fieldset>
				<legend>Commande refusée</legend>
				<h2>Solde insuffisant</h2>
				<br /> <a href="gestionCompteClient">Recharger votre compte ici</a>
			</fieldset>
		</c:if>
		<c:if test="${BonI=='1'}">
			<fieldset>
				<legend>Commande refusée</legend>
				<h2>Bon non valide</h2><br/>
	    	</fieldset>
	    </c:if>
	    <c:if test="${telechargement=='1'}">
	    	<fieldset>
				<legend>Commande acceptée</legend>
				<h2>Félicitation vous venez d'acheter ${film.titre}!</h2><br/>
				<h2>Vous pouvez télécharger le film <a href="telecharger.pdf?titre=${film.titre}">ici</a></h2>
	    	</fieldset>
	    </c:if>
	    <c:if test="${streaming=='1'}">
	    	<fieldset>
				<legend>Commande acceptée</legend>
				<h2>Félicitation vous venez de louer ${film.titre}!</h2><br/>
				<h2>Vous pouvez visionner le film <a href="streaming?titre=${film.titre}&expi=${expi}">ici</a></h2>
				<p>Ce lien sera actif pendant 48h et vous pouvez le retrouver via la 
					<a href="gestionCompteClient">Gestion du compte</a>
				</p>
	    	</fieldset>
	    </c:if>
	    <fieldset>
	    	<legend>Description</legend>
			<center>
				<h2>${film.titre}</h2>
			</center>
			<table >
				<tr>
					<td>Ann&eacutee :</td>
					<td>${film.annee}</td>
				</tr>
				<tr>
					<td>Acteurs :</td>
					<td><c:forEach var="act" items="${film.tabActeur}">${act.prenom} ${act.nom} , 
							</c:forEach></td>
				</tr>
				<tr>
					<td WIDTH=260 HEIGHT=50>Description :</td>
					<td>${film.description}</td>
				</tr>
			</table>
		</fieldset>
		<fieldset>
			<legend>Tarif</legend>
			<ul>
				<li>Achat : 20</li>
				<li>Location(48h) : 5</li>
			</ul>
	    </fieldset>
		<fieldset>
			<legend>Commander</legend>
		 	<form method="POST" action="ficheFilm">
		 		<br/><h3>Votre solde est de : ${user.solde}</h3><br/>
				<label for="bonLab">Utiliser un bon d'achat : </label> 
				<input name="codeBon" type="text" id="codeBon" /> <br><br>
				<p>Si le montant est inférieur à celui du bon, le reste sera versé sur votre compte</p>
				<input type="submit" value="Acheter" name="achat" />
				<input type="submit" value="Louer" name="achat" />
			</form>
		 </fieldset>

	</div>
</div>
</body>
</html>
