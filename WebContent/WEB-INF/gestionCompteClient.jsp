<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="styles/menuStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/connexionStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/bodyContentStyle.css" />
<link type="text/css" rel="stylesheet"
	href="styles/border.css" />
<title>Gestion compte client</title>
</head>
<body>
	<div class="bodyContent">
	<c:import url="../menus/menuClient.jsp"></c:import>	
	<div class = "content">
	<form class="Compte" method="POST" action="gestionCompteClient">
		<fieldset>
			<legend>Informations du Compte</legend>
			<ul>
				<li>Pseudo : ${user.pseudo}</li>
				<li>Nom : ${user.nom}</li>
				<li>Prenom : ${user.prenom}</li>
				<li>Mot de passe : ${user.motDePasse}</li>
			</ul>
    	</fieldset>
		<fieldset>
			<legend>Solde</legend>
			<ul>
				<li>Solde : ${user.solde}</li>
			</ul>
			<p>Recharger le compte :</p>
			<select name="montant" size=1>
				<option value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option value="40">40</option>
				<option value="50">50</option>
				<option value="60">60</option>
				<option value="70">70</option>
				<option value="80">80</option>
				<option value="90">90</option>
				<option value="100">100</option>
				<option value="150">150</option>
				<option value="200">200</option>
			</select>
			<input value="Recharger" tabindex="12" type="submit" id="recharger"></input>
		</fieldset>
		<fieldset>
			<legend>Films loués</legend>
			<ul>
				<c:forEach var="streaming" items="${filmLoue}">
					<li>Film : <a href="streaming?titre=${streaming[0]}&expi=${streaming[1]}">${streaming[0]}</a>   ,  Temps restant :  ${streaming[2]} H ${streaming[3]} min </li>
				</c:forEach>
			</ul>
    	</fieldset>
		<fieldset>
			<legend>Historique des achats, locations et bon prepayé</legend>
			<ul>
				<c:forEach var="vente" items="${ventesUser}">
					<li>Type : ${vente[0]} | Date : ${vente[1]} | Prix : ${vente[2]} | Film : ${vente[3]}</li>
				</c:forEach>
			</ul>
    	</fieldset>
	</form>
	</div>
	</div>
</body>
</html>