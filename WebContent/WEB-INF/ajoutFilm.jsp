<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="styles/menuStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/bodyContentStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/catalogueStyle.css" />
<title>Ajout Film</title>
</head>
<body>

		<div class="bodyContent">
		<c:import url="../menus/menuAdmin.jsp"></c:import>
	
	
		<div class="contentWoCenter">
				<form method="POST" action="ajoutFilm">
		<fieldset>
			<legend>Informations sur le film</legend>
			<label for="titre" style="display: block; width: 50px; float: left;">Titre
				:</label><input  name ="titre" type="text"  /><br /> <label for="annee"
				style="display: block; width: 50px; float: left;">Ann&eacutee
				:</label><input type="text" name="annee" /><br /> <label for="duree"
				style="display: block; width: 50px; float: left;">Dur&eacutee
				:</label><input type="text" name="duree" /><br />
			<fieldset>
				<legend>Acteurs</legend>
				<br /> <label for="acteur1">Acteur :</label><br /> <label
					for="acteur1">Pr&eacutenom :</label><input type="text"
					name="nomActeur1" /> <label for="acteur1">Nom :</label><input
					type="text" name="prenomActeur1" /> <label for="acteur1">Role
					:</label><input type="text" name="roleActeur1" /><br /> <br /> <label
					for="acteur2">Acteur :</label><br /> <label for="acteur2">Pr&eacutenom
					:</label><input type="text" name="nomActeur2" /> <label for="acteur2">Nom
					:</label><input type="text" name="prenomActeur2" /> <label for="acteur1">Role
					:</label><input type="text" name="roleActeur1" /><br /> <br />
			</fieldset>

			<fieldset>
				<legend>R&eacutealisateurs</legend>
				<br /> <label for="Realisateur1">R&eacutealisateur :</label><br />
				<label for="Realisateur1">Pr&eacutenom :</label><input type="text"
					name="nomRealisateur1" /> <label for="Realisateur1">Nom :</label><input
					type="text" name="prenomRealisateur1" /><br /> <br /> <label
					for="Realisateur2">R&eacutealisateur :</label><br /> <label
					for="Realisateur2">Pr&eacutenom :</label><input type="text"
					name="nomRealisateur2" /> <label for="Realisateur2">Nom :</label><input
					type="text" name="prenomRealisateur2" /><br /> <br />
			</fieldset>
			<fieldset>
				<legend>Autres titres </legend>
				<br /> 
				<label for="Fr">Titre français :</label><input type="text"
					name="titreFr" /> 
			<label for="De">Titre allemands :</label><input type="text"
					name="titreDe" /> 		
					<label for="It">Titre italien :</label><input type="text"
					name="titreIt" /> 
			</fieldset>
			<fieldset>
						
				<legend>Resumé</legend>
				<br />
				<textarea rows="6" cols="100" name="description"></textarea>
				<br />
			</fieldset>
			<br />
			
				<input value="Ajouter" tabindex="4" type="submit" id="u_0_d"></input>
		</fieldset>
	</form>
	</div>
	</div>
</body>
</html>