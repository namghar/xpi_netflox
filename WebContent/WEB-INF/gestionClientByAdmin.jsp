<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="styles/menuStyle.css" />
<link type="text/css" rel="stylesheet"
	href="styles/gestionUtilisateurStyle.css" />
<link type="text/css" rel="stylesheet" href="styles/bodyContentStyle.css" />

<title>Gestion compte client</title>
</head>
<body>
	<div class="bodyContent">
	<c:import url="../menus/menuAdmin.jsp"></c:import> 
		<div class="content">
		<form method="post" action="gestioncomptes">
			<h3>Suppression de clients :</h3>
			<table class="userInfo" title="Suppression de clients :">

				<tr>
					<th>Pseudo
					<th />
					<th>Nom
					<th />
					<th>Prenom
					<th />
					<th>Mot de passe
					<th />
					<th>Solde
					<th />
					<th>
					<th />
				</tr>


				<c:forEach var="user" items="${userList}">
					<tr>
						<td><c:out value="${user.pseudo}" />
						<td />
						<td><c:out value="${user.nom}" />
						<td />
						<td><c:out value="${user.prenom}" />
						<td />
						<td><c:out value="${user.motDePasse}" />
						<td />
						<td><c:out value="${user.solde}" />
						<td />
						<td><c:choose>
								<c:when test="${user.id == 1}">
									<input type="checkbox" disabled="disabled" id="${user.id}"
										name="${user.id}" value="${user.id}">
								</c:when>
								<c:otherwise>
									<input type="checkbox" id="${user.id}" name="${user.id}"
										value="${user.id}">
								</c:otherwise>
							</c:choose>
						<td />
					</tr>

				</c:forEach>
				<tr class="sendButton">
					<td><input type="submit" id="supprimer" name="supprimer"
						value="Supprimer selection"></td>
				</tr>
			</table>

		</form>

		<form method="post" action="ajoutclient">
			<h3>Ajout de clients :</h3>
			<table title="Ajout de clients :">
			
				<tr>
					<th>Pseudo
					<th />
					<th>Nom
					<th />
					<th>Prenom
					<th />
					<th>Mot de passe
					<th />
					<th>Solde
					<th />
					
				</tr>
				
				<tr>
					<td><input type="text" id="pseudo" name="pseudo" value="">
					<td />
					<td><input type="text" id="nom" name="nom" value="">
					<td />
					<td><input type="text" id="prenom" name="prenom" value="">
					<td />
					<td><input type="text" id="motdepasse" name="motdepasse" value="">
					<td />
					<td><input type="text" id="solde" name="solde" value="">
					<td />
					


				</tr>
				<tr class="sendButton">
					<td><input type="submit" id="ajout" name="ajout"
						value="Ajouter ce client"></td>
				</tr>
			</table>

		</form>
		
		<c:if test="${!empty forms.erreurs}"> 
		<div class = "erreurContainer">
			<span class="erreur">${forms.erreurs['pseudo']}</span>
			<span class="erreur">${forms.erreurs['nom']}</span>
			<span class="erreur">${forms.erreurs['prenom']}</span>
			<span class="erreur">${forms.erreurs['motdepasse']}</span>
			<span class="erreur">${forms.erreurs['solde']}</span>
			<p class="${empty forms.erreurs ? 'succes' : 'erreur'}">${forms.resultat}</p>
		</div>
			
         </c:if>    
             
</div>
	</div>

</body>
</html>