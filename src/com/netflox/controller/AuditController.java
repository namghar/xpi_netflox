package com.netflox.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netflox.model.DownloadHelper;
import com.netflox.pdf.pdfCreator;

public class AuditController extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7588244331237665185L;
	private static final String IN_PATH = "/res/data";
	private static final String OUT_PATH = "";
	private String AUDIT_PDF = "audit_output.pdf";

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Affichage de la page d'inscription */
		pdfCreator pdfc = new pdfCreator();
		
		String inPath = this.getServletContext().getRealPath(IN_PATH);
		String outPath = this.getServletContext().getRealPath(OUT_PATH);
		
		pdfc.createAuditPdf(inPath, outPath);
		
		DownloadHelper dh = new DownloadHelper();
		dh.downloadLink(request, response, outPath, AUDIT_PDF);
		//this.getServletContext().getRequestDispatcher(VUE).
		//.forward(request, response);
	}
}
