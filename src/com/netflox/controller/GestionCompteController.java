package com.netflox.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netflox.dao.MysqlGestionUtilisateur;
import com.netflox.form.GestionUtilisateur;
import com.netflox.model.Film;
import com.netflox.model.FilmsManager;
import com.netflox.model.Utilisateur;

public class GestionCompteController extends HttpServlet {

	private static final String VUE = "/WEB-INF/gestionClientByAdmin.jsp";
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
				
		MysqlGestionUtilisateur mgu = new MysqlGestionUtilisateur();
		ArrayList<Utilisateur> users =  mgu.getAllUsers();
				
		request.setAttribute("userList", users);
			
		
		this.getServletContext().getRequestDispatcher(VUE)
		.forward(request, response);
	}


	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		

		MysqlGestionUtilisateur mgu = new MysqlGestionUtilisateur();
		ArrayList<Utilisateur> users =  mgu.getAllUsers();
		
	
		request.setAttribute("userList", users);
		

		GestionUtilisateur gu = new GestionUtilisateur();
		gu.supprimerListUtilisateur(request, users);
		
		this.getServletContext().getRequestDispatcher(VUE)
		.forward(request, response);
	}
	
}
