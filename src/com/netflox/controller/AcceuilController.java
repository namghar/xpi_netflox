package com.netflox.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netflox.form.Connexion;

public class AcceuilController extends HttpServlet {

	public static final String VUE = "/WEB-INF/acceuil.jsp";
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher(VUE)
		.forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		this.getServletContext().getRequestDispatcher(VUE)
		.forward(request, response);
	}
}
