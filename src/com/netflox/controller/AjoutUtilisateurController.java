package com.netflox.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.netflox.form.AjoutUtilisateur;
import com.netflox.form.Inscription;
import com.netflox.model.Utilisateur;

public class AjoutUtilisateurController extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5066589631579402450L;
	public static final String ATT_USER = "user";

	public static final String VUE = "/gestioncomptes";

	public static final String ATT_FORM = "forms";	
	


	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Affichage de la page d'inscription */
		this.getServletContext().getRequestDispatcher(VUE)
		.forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Préparation de l'objet formulaire */
		AjoutUtilisateur form = new AjoutUtilisateur();

		/*
		 * Appel au traitement et à la validation de la requête, et récupération
		 * du bean en résultant
		 */

		Utilisateur utilisateur = form.inscrireUtilisateur(request);

		/* Stockage du formulaire et du bean dans l'objet request */
		request.setAttribute(ATT_FORM, form);
		request.setAttribute(ATT_USER, utilisateur);

		this.getServletContext().getRequestDispatcher(VUE)
		.forward(request, response);
	}
}
