package com.netflox.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.netflox.form.Achat;
import com.netflox.model.Utilisateur;

public class BonController extends HttpServlet{
	
	public static final String ATT_USER         = "utilisateur";
	public static final String ATT_FORM         = "form";
	public static final String ATT_SESSION_USER = "sessionUtilisateur";
	public static final String VUE = "/WEB-INF/Bon.jsp";
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("codeBon", 0);
		request.setAttribute("SoldeI", 0);
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();
		//récupération du user
		Utilisateur user = (Utilisateur) session.getAttribute(ATT_SESSION_USER);
		Achat achat = new Achat(user);
		request.setAttribute("codeBon", 0);
		request.setAttribute("SoldeI", 0);
		//récupération du montant
		int montant = Integer.parseInt(request.getParameter("montant"));
		
		String codeBon = achat.bonPrepaye(montant);
		if(codeBon != null){
			//donner bon
			request.setAttribute("codeBon", codeBon);
		}else{
			request.setAttribute("SoldeI", 1);
		}
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}
	
}
