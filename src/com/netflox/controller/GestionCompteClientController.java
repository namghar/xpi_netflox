package com.netflox.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.netflox.dao.MysqlGestionUtilisateur;
import com.netflox.form.GestionCompteClient;
import com.netflox.model.FilmsManager;
import com.netflox.model.Streaming;
import com.netflox.model.Utilisateur;
import com.netflox.model.Vente;

public class GestionCompteClientController extends HttpServlet {
	
	public static final String ATT_USER         = "utilisateur";
	public static final String ATT_FORM         = "form";
	public static final String ATT_SESSION_USER = "sessionUtilisateur";
	public static final String VUE = "/WEB-INF/gestionCompteClient.jsp";
	private final String IN_PATH = "/res/data";
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();
		//récupération du user
		Utilisateur user = (Utilisateur) session.getAttribute(ATT_SESSION_USER);
		MysqlGestionUtilisateur BDu = new MysqlGestionUtilisateur();
		user.setSolde(BDu.getSoldeUtilisateur(user));
		request.setAttribute("user", user);
		
		String inPath = this.getServletContext().getRealPath(IN_PATH);
		FilmsManager films = new FilmsManager(inPath);
		
		GestionCompteClient gcc = new GestionCompteClient(user);
		//récupération film loué
		ArrayList<String[]> filmLoue = new ArrayList<String[]>();
		ArrayList<Streaming> stream = gcc.filmLoue();
		for(Streaming s : stream){
			String titre = films.getFilm(s.getFilm().getId()).getTitre();
			String[] t = {titre , Long.toString(s.getDateFin()) , Long.toString(s.tempsRestantH()) , Long.toString(s.tempsRestantM()) };
			filmLoue.add(t);
		}
		request.setAttribute("filmLoue", filmLoue);
		//récupération historique achat
		ArrayList<String[]> ventesUser = new ArrayList<String[]>();
		ArrayList<Vente> histo = gcc.histoAchat();
		for(Vente v: histo){
			String type;
			if(v.getType().equals('a')){type="Achat";}else if(v.getType().equals('l')){type="Location";}else{type="Bon";}
			Date d = new Date(v.getDate());
			String prix = Integer.toString(v.getPrix());
			String titre = "";
			if(!type.equals("Bon")){
				titre = films.getFilm(v.getIdFilm()).getTitre();
			}
			String[] tab = {type,d.toString(),prix,titre};
			ventesUser.add(tab);
		}
		request.setAttribute("ventesUser", ventesUser);
		
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Récupération de la session depuis la requête */
		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();
		//récupération du user
		Utilisateur user = (Utilisateur) session.getAttribute(ATT_SESSION_USER);
		MysqlGestionUtilisateur BDu = new MysqlGestionUtilisateur();
		user.setSolde(BDu.getSoldeUtilisateur(user));
		request.setAttribute("user", user);
		
		String inPath = this.getServletContext().getRealPath(IN_PATH);
		FilmsManager films = new FilmsManager(inPath);
		
		GestionCompteClient gcc = new GestionCompteClient(user);
		//récupération film loué
		ArrayList<String[]> filmLoue = new ArrayList<String[]>();
		ArrayList<Streaming> stream = gcc.filmLoue();
		for(Streaming s : stream){
			String titre = films.getFilm(s.getFilm().getId()).getTitre();
			String[] t = {titre , Long.toString(s.tempsRestantH()) , Long.toString(s.tempsRestantM()) };
			filmLoue.add(t);
		}
		request.setAttribute("filmLoue", filmLoue);
		//récupération historique achat
		ArrayList<String[]> ventesUser = new ArrayList<String[]>();
		ArrayList<Vente> histo = gcc.histoAchat();
		for(Vente v: histo){
			String type;
			if(v.getType().equals('a')){type="Achat";}else if(v.getType().equals('l')){type="Location";}else{type="Bon";}
			Date d = new Date(v.getDate());
			String prix = Integer.toString(v.getPrix());
			String titre = "";
			if(!type.equals("Bon")){
				titre = films.getFilm(v.getIdFilm()).getTitre();
			}
			String[] tab = {type,d.toString(),prix,titre};
			ventesUser.add(tab);
		}
		request.setAttribute("ventesUser", ventesUser);
		//rechargement compte
		//récupération du montant
		int montant = Integer.parseInt(request.getParameter("montant"));
		int newSolde = gcc.rechargerCompte(montant);
		user.setSolde(newSolde);
		
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

}
