package com.netflox.controller;



import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netflox.model.Film;
import com.netflox.model.FilmsManager;

public class CatalogueController extends HttpServlet {

	private static final String VUE = "/WEB-INF/catalogue.jsp";
	private static final String IN_PATH = "/res/data";
	

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
//		Recuperer l'adresse du movies.xml 
		String inPath = this.getServletContext().getRealPath(IN_PATH);
		int page = 1;
		int recordsPerPage = 20;
		if (request.getParameter("page") != null)
			page = Integer.parseInt(request.getParameter("page"));
		FilmsManager films = new FilmsManager(inPath);
		List<Film> list = films.FilmsPerPage((page - 1),
				recordsPerPage, "T");
		int noOfRecords = films.getNoOfFilms();
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
//		Envoie des informations du film à la jsp
		request.setAttribute("FilmsList", list);
		request.setAttribute("noOfPages", noOfPages);
		request.setAttribute("currentPage", page);
		request.setAttribute("ordre", "dans l'ordre alphabetique");
		System.out.println(request.getSession().getAttribute("nameCl")+"tttt");
		this.getServletContext().getRequestDispatcher(VUE)
		.forward(request, response);
	}


	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
//		Recuperer l'adresse du movies.xml 
		String inPath = this.getServletContext().getRealPath(IN_PATH);
		int page = 1;
		int recordsPerPage = 20;
		if (request.getParameter("page") != null)
			page = Integer.parseInt(request.getParameter("page"));
		FilmsManager films = new FilmsManager(inPath);
		List<Film> list = films.FilmsPerPage((page - 1),
				recordsPerPage, "T");
		int noOfRecords = films.getNoOfFilms();
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
//		Envoie des informations du film à la jsp
		request.setAttribute("FilmsList", list);
		request.setAttribute("noOfPages", noOfPages);
		request.setAttribute("currentPage", page);
		request.setAttribute("ordre", "dans l'ordre alphabetique");
		
		
		this.getServletContext().getRequestDispatcher(VUE)
		.forward(request, response);
	}
}
