package com.netflox.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netflox.model.DownloadHelper;
import com.netflox.pdf.pdfCreator;

public class GeneratePdfController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3628839781153159886L;
	//private static final String VUE = "/WEB-INF/acceuil.jsp";
	private static final String IN_PATH = "/res/data";
	private static final String OUT_PATH = "";
	private String MOVIES_PDF = "movies_output.pdf";

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Affichage de la page d'inscription */
		pdfCreator pdfc = new pdfCreator();
		
		String inPath = this.getServletContext().getRealPath(IN_PATH);
		String outPath = this.getServletContext().getRealPath(OUT_PATH);
		
		pdfc.createMoviesPdf(inPath, outPath);
		
		DownloadHelper dh = new DownloadHelper();
		dh.downloadLink(request, response, outPath, MOVIES_PDF);
		//this.getServletContext().getRequestDispatcher(VUE).
		//.forward(request, response);
	}
	
	
	
}
