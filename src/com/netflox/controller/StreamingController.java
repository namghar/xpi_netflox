package com.netflox.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.netflox.dao.MysqlGestionUtilisateur;
import com.netflox.form.Achat;
import com.netflox.model.Bon;
import com.netflox.model.DownloadHelper;
import com.netflox.model.Film;
import com.netflox.model.FilmsManager;
import com.netflox.model.Utilisateur;
import com.netflox.pdf.pdfCreator;

public class StreamingController extends HttpServlet {

	
	private static final String IN_PATH = "/res/data";
	private static final String OUT_PATH = "";
	private String MOVIES_PDF = "movies_output.pdf";
	public final String ATT_USER         = "utilisateur";
	public final String ATT_FORM         = "form";
	public final String ATT_SESSION_USER = "sessionUtilisateur";
	private final String VUE = "/WEB-INF/streaming.jsp";
	private final String video = "/res/video/KFPanda.mp4";
	private Film f;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();
		Utilisateur user = (Utilisateur) session.getAttribute(ATT_SESSION_USER);
		
		
		String inPath = this.getServletContext().getRealPath(IN_PATH);
		FilmsManager films = new FilmsManager(inPath);
		String titre = request.getParameter("titre");
		
		if (titre != null){
			byte ptext[] = titre.getBytes();
			titre = new String(ptext, "UTF-8");
			System.out.println(titre);
			List<Film> list = films.searchResult(titre, "Titre");
			request.setAttribute("film", list.get(0));
			f = list.get(0);
		}
		//gestion temps
		Long dateExpi = Long.parseLong(request.getParameter("expi"));
		session.setMaxInactiveInterval((int)((dateExpi - System.currentTimeMillis())/1000));
		
		request.setAttribute("titre", titre);
		request.setAttribute("video", this.getServletContext().getRealPath(video));
		
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();
		//récupération du user
		Utilisateur user = (Utilisateur) session.getAttribute(ATT_SESSION_USER);
		//session.
		
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}
	
	
	
}