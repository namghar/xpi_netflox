package com.netflox.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.netflox.dao.MysqlGestionUtilisateur;
import com.netflox.form.Achat;
import com.netflox.model.Bon;
import com.netflox.model.Film;
import com.netflox.model.FilmsManager;
import com.netflox.model.Utilisateur;

public class AchatController extends HttpServlet{
	public final String ATT_USER         = "utilisateur";
	public final String ATT_FORM         = "form";
	public final String ATT_SESSION_USER = "sessionUtilisateur";
	private final long serialVersionUID = 1L;
	private final String VUE = "/WEB-INF/ficheFilm.jsp";
	private final String IN_PATH = "/res/data";
	private Film f;
	private final long dureeLoc = 48*60*60*1000;

	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();
		Utilisateur user = (Utilisateur) session.getAttribute(ATT_SESSION_USER);
		MysqlGestionUtilisateur BDu = new MysqlGestionUtilisateur();
		user.setSolde(BDu.getSoldeUtilisateur(user));
		String inPath = this.getServletContext().getRealPath(IN_PATH);
		FilmsManager films = new FilmsManager(inPath);
		String titre = request.getParameter("titre");
		if (titre != null){
			byte ptext[] = titre.getBytes();
			titre = new String(ptext, "UTF-8");
			System.out.println(titre);
			List<Film> list = films.searchResult(titre, "Titre");
			request.setAttribute("film", list.get(0));
			f = list.get(0);
		}
		String codeBon = "";
		request.setAttribute("codeBon", codeBon);
		request.setAttribute("SoldeI", 0);
		request.setAttribute("BonI", 0);
		request.setAttribute("telechargement", 0);
		request.setAttribute("user", user);
		request.setAttribute("streaming", 0);
		request.setAttribute("expi", "0");
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			

		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();
		//récupération du user
		Utilisateur user = (Utilisateur) session.getAttribute(ATT_SESSION_USER);
		MysqlGestionUtilisateur BDu = new MysqlGestionUtilisateur();
		user.setSolde(BDu.getSoldeUtilisateur(user));
		request.setAttribute("film", f);
		request.setAttribute("SoldeI", 0);
		request.setAttribute("BonI", 0);
		request.setAttribute("telechargement", 0);
		request.setAttribute("user", user);
		request.setAttribute("streaming", 0);
		request.setAttribute("expi"," 0");
		
		Achat achat = new Achat(user);
		
		//vérifier si le bon existe et qu'il est valide
		//S'il est valide on le donne en paramètre à achat.acheter
		String codeBon = request.getParameter("codeBon");
		Bon b = achat.verifBon(codeBon);
		if((b.getId() == 0) && (codeBon.length() > 0)){
			request.setAttribute("BonI", 1);
		}else{
			//choix entre acheter et louer
			if(request.getParameter("achat").equalsIgnoreCase("Acheter")){
				if(!achat.acheter(f.getId(), b)){
					request.setAttribute("SoldeI", 1);
				}else{
					request.setAttribute("telechargement", 1);
				}
			}else{
				if(!achat.louer(f.getId(), b)){
					request.setAttribute("SoldeI", 1);
				}else{
					request.setAttribute("streaming", 1);
					request.setAttribute("expi", Long.toString(System.currentTimeMillis() + dureeLoc) );
				}
			}
		}
		user.setSolde(BDu.getSoldeUtilisateur(user));
		request.setAttribute("user", user);
		
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}
	
}
