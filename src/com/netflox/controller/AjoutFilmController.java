package com.netflox.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netflox.model.Acteur;
import com.netflox.model.AlternateTitle;
import com.netflox.model.Film;
import com.netflox.model.FilmsManager;
import com.netflox.model.Realisateur;

/**
 * Servlet implementation class AjoutFilmController
 */
@WebServlet("/AjoutFilmController")
public class AjoutFilmController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String VUE = "/WEB-INF/ajoutFilm.jsp";
	private static final String IN_PATH = "/res/data";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjoutFilmController() {
        super();
        // TODO Auto-generated constructor stub
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);

	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String inPath = this.getServletContext().getRealPath(IN_PATH);

//		request.getParameter("titre");
		FilmsManager p = new FilmsManager(inPath);
		List<Acteur> a = new ArrayList<Acteur>();
		a.add(new Acteur(request.getParameter("nomActeur1"), request.getParameter("prenomActeur1"),request.getParameter("role")));
		a.add(new Acteur(request.getParameter("nomActeur2"), request.getParameter("prenomActeur2"),request.getParameter("role")));
		List<Realisateur> r = new ArrayList<Realisateur>();
		r.add(new Realisateur(request.getParameter("nomRealisateur1"), request.getParameter("prenomRealisateur1")));
		r.add(new Realisateur(request.getParameter("nomRealisateur1"), request.getParameter("prenomRealisateur1")));
		List<AlternateTitle> at = new ArrayList<AlternateTitle>();
		at.add(new AlternateTitle("fr",request.getParameter( "titreFr")));
		at.add(new AlternateTitle("de", request.getParameter("titreDe")));
		at.add(new AlternateTitle("it", request.getParameter("titreIt")));
		System.out.println(request.getParameter("titre"));
		p.ajouterFilm(p.getId(), 
				request.getParameter("titre"),
				request.getParameter("annee"),
				request.getParameter("duree"),
				request.getParameter("description"),
				r, a, at);
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);

	}

}
