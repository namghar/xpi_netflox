package com.netflox.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.netflox.form.Connexion;
import com.netflox.model.Utilisateur;

public class ConnexionController extends HttpServlet {
	public static final String ATT_USER         = "utilisateur";
	public static final String ATT_FORM         = "form";
	public static final String ATT_SESSION_USER = "sessionUtilisateur";
	public static final String VUE = "/WEB-INF/acceuil.jsp";

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Affichage de la page d'inscription */
		this.getServletContext().getRequestDispatcher(VUE)
		.forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Préparation de l'objet formulaire */
		Connexion form = new Connexion();		

		/*
		 * Appel au traitement et à la validation de la requête, et récupération
		 * du bean en résultant
		 */
		Utilisateur utilisateur = form.connectionUtilisateur(request);
        

		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();

		/**
		 * Si aucune erreur de validation n'a eu lieu, alors ajout du bean
		 * Utilisateur à la session, sinon suppression du bean de la session.
		 */
		if ( form.getErreurs().isEmpty() ) {
			session.setAttribute( ATT_SESSION_USER, utilisateur );
		} else {
			session.setAttribute( ATT_SESSION_USER, null );
		}

		/* Stockage du formulaire et du bean dans l'objet request */
//		System.out.println(utilisateur.getPseudo());
		session.setAttribute("nameCl", utilisateur.getPseudo());
		request.setAttribute( ATT_FORM, form );
		request.setAttribute( ATT_USER, utilisateur );	

		this.getServletContext().getRequestDispatcher(VUE)
		.forward(request, response);
	}
}
