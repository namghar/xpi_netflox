package com.netflox.form;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import com.netflox.dao.MysqlGestionUtilisateur;
import com.netflox.model.Utilisateur;

public class GestionUtilisateur {

	public void supprimerListUtilisateur(HttpServletRequest request, ArrayList<Utilisateur> allUsers){
		
		if(getValeurChamp(request, "supprimer") != null){
			
		ArrayList<Utilisateur> delList = new ArrayList<Utilisateur>();
		
		for(Utilisateur user:allUsers){
			String sid = String.valueOf(user.getId());
			String t = getValeurChamp(request, sid);
			if(t != null && t.equals(sid))
				delList.add(user);	
		}
		
		MysqlGestionUtilisateur mgu = new MysqlGestionUtilisateur();
		mgu.deleteListUsers(delList);
		}
	}
	
	private static String getValeurChamp(HttpServletRequest request,
			String nomChamp) {
		String valeur = request.getParameter(nomChamp);
		if (valeur == null || valeur.trim().length() == 0) {
			return null;
		} else {
			return valeur.trim();
		}
	}
}
