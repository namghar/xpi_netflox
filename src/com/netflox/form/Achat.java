package com.netflox.form;


import java.util.ArrayList;

import com.netflox.model.Utilisateur;
import com.netflox.model.Vente;
import com.netflox.model.Bon;
import com.netflox.dao.MysqlGestionUtilisateur;
import com.netflox.dao.MysqlGestionVente;

public class Achat {

	private Utilisateur user;
	private int PrixFilm = 20;
	private int PrixLoc = 5;
	private long date;
	
	public Achat(Utilisateur utilisateur){
		user = utilisateur;
		date = System.currentTimeMillis();
	}
	
	public int getSolde(){
		MysqlGestionUtilisateur db = new MysqlGestionUtilisateur();
		return db.getSoldeUtilisateur(user);
	}
	
	/*
	 * return false si le solde du client est insuffisant dans ce cas prevenir le client par un message
	 * et lui proposer de recharcher son compte en lui precisant son solde
	 */
	public boolean acheter(String idFilm, Bon b){
		int solde = getSolde();
		MysqlGestionUtilisateur db = new MysqlGestionUtilisateur();
		MysqlGestionVente venteBD = new MysqlGestionVente();
		//si bon valide
		if(b.getId() != 0){
			if((solde + b.getMontant()) >= PrixFilm){
				//gestion BD vente
				Vente v = new Vente();
				v.setIdFilm(idFilm);
				v.setIdClient(user.getId());
				v.setPrix(PrixFilm);
				v.setType("a");
				date = System.currentTimeMillis();
				v.setDate(date);
				int id = venteBD.ajouterVente(v);
				if(id != -1){
					db.updateSolde(user, (b.getMontant()-PrixFilm));
					v.setId(id);
					//Mise a jour Bon
					b.setDateU(date);
					b.setIdClientU(user.getId());
					b.setUtilise(1);
					b.setIdFilm(idFilm);
					b.setType("a");
					venteBD.updateBon(b);
					return true;
				}
			}
		}else{
			if(solde >= PrixFilm){
				//gestion BD vente
				Vente v = new Vente();
				v.setIdFilm(idFilm);
				v.setIdClient(user.getId());
				v.setPrix(PrixFilm);
				v.setType("a");
				date = System.currentTimeMillis();
				v.setDate(date);
				int id = venteBD.ajouterVente(v);
				if(id != -1){
					db.updateSolde(user, -PrixFilm);
					v.setId(id);
					return true;
				}
			}
		}
		return false;
	}
	
	/*
	 * return false si le solde du client est insuffisant dans ce cas prevenir le client par un message
	 * et lui proposer de recharcher son compte en lui precisant son solde
	 */
	public boolean louer(String idFilm, Bon b){
		int solde = getSolde();
		MysqlGestionUtilisateur db = new MysqlGestionUtilisateur();
		MysqlGestionVente venteBD = new MysqlGestionVente();
		//si bon valide
		if(b.getId() != 0){
			if((solde + b.getMontant()) >= PrixLoc){		
				//gestion BD vente
				Vente v = new Vente();
				v.setIdFilm(idFilm);
				v.setIdClient(user.getId());
				v.setPrix(PrixLoc);
				v.setType("l");
				date = System.currentTimeMillis();
				v.setDate(date);
				int id = venteBD.ajouterVente(v);
				if(id != -1){
					db.updateSolde(user, (b.getMontant()-PrixLoc));
					v.setId(id);
					//Mise a jour Bon
					b.setDateU(date);
					b.setIdClientU(user.getId());
					b.setUtilise(1);
					b.setIdFilm(idFilm);
					b.setType("l");
					venteBD.updateBon(b);
					return true;
				}
			}
		}else{
			if(solde >= PrixLoc){		
				//gestion BD vente
				Vente v = new Vente();
				v.setIdFilm(idFilm);
				v.setIdClient(user.getId());
				v.setPrix(PrixLoc);
				v.setType("l");
				date = System.currentTimeMillis();
				v.setDate(date);
				int id = venteBD.ajouterVente(v);
				if(id != -1){
					db.updateSolde(user, -PrixLoc);
					v.setId(id);
					return true;
				}
			}
		}
		return false;
	}
	
	/*
	 * return false si le solde du client est insuffisant dans ce cas prevenir le client par un message
	 * et lui proposer de recharcher son compte en lui precisant son solde
	 */
	public String bonPrepaye(int valeur){
		int solde = getSolde();
		MysqlGestionUtilisateur db = new MysqlGestionUtilisateur();
		MysqlGestionVente venteBD = new MysqlGestionVente();
		if(solde >= valeur){
			//gestion BD bon et vente
			Vente v = new Vente();
			v.setIdClient(user.getId());
			v.setPrix(valeur);
			v.setType("b");
			v.setDate(date); //TODO
			int id = venteBD.ajouterVente(v);
			if(id != -1){
				db.updateSolde(user, -valeur);
				v.setId(id);
				Bon b = new Bon();
				b.setId(id);
				b.setMontant(valeur);
				b.genererCode();
				venteBD.ajouterBon(b);
				return b.getCodeBon();
			}
		}
		return null;
	}
	
	public ArrayList<String> getFilmVendu(){
		MysqlGestionVente venteBD = new MysqlGestionVente();
		return venteBD.filmsInfosVenteLoueBon("A");
	}
	
	//retourne bon vide si non valide
	public Bon verifBon(String code){
		Bon b = new Bon();
		String[] c = code.split("NetFLOX##");
		//vérification du code
		if(c.length != 2){
			System.out.println("0");
			return b;
		}
		int montant = 0;
		int id = 0;
		try{
			montant = Integer.parseInt(c[0]);
			id = Integer.parseInt(c[1]);
		}catch(NumberFormatException e){
			System.out.println("1");
			return b;
		}
		if((montant != 10) && (montant != 20) && (montant != 30) && (montant != 40) && (montant != 50)){
			System.out.println("2");
			return b;
		}
		if(id <= 0){
			System.out.println("3");
			return b;
		}
		MysqlGestionVente venteBD = new MysqlGestionVente();
		
		if(venteBD.bonValide(id) != montant){
			System.out.println("4");
			return b;
		}
		b.setId(id);
		b.setMontant(montant);
		b.setUtilise(0);
		System.out.println("5");
		return b;
	}
}
