package com.netflox.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.netflox.dao.MysqlGestionUtilisateur;
import com.netflox.model.Utilisateur;

public class AjoutUtilisateur {

	private static final String CHAMP_PSEUDO = "pseudo";
	private static final String CHAMP_PASS = "motdepasse";	
	private static final String CHAMP_NOM = "nom";
	private static final String CHAMP_PRENOM = "prenom";
	private static final String CHAMP_SOLDE = "solde";
	private static final String EXIST_USER = "userexist";


	private String resultat;
	private Map<String, String> erreurs = new HashMap<String, String>();

	public String getResultat() {
		return resultat;
	}

	public Map<String, String> getErreurs() {
		return erreurs;
	}

	public Utilisateur inscrireUtilisateur(HttpServletRequest request) {
		String pseudo = getValeurChamp(request, CHAMP_PSEUDO);
		String motDePasse = getValeurChamp(request, CHAMP_PASS);	
		String nom = getValeurChamp(request, CHAMP_NOM);
		String prenom = getValeurChamp(request, CHAMP_PRENOM);
		String solde = getValeurChamp(request, CHAMP_SOLDE);
		
		Utilisateur utilisateur = new Utilisateur();

		try {
			validationPseudo(pseudo);
		} catch (Exception e) {
			setErreur(CHAMP_PSEUDO, e.getMessage());
		}
		utilisateur.setPseudo(pseudo);

		try {
			validationMotsDePasse(motDePasse);
		} catch (Exception e) {
			setErreur(CHAMP_PASS, e.getMessage());			
		}
		utilisateur.setMotDePasse(motDePasse);

		try {
			validationNom(nom);
		} catch (Exception e) {
			setErreur(CHAMP_NOM, e.getMessage());
		}
		utilisateur.setNom(nom);

		try {
			validationPrenom(prenom);
		} catch (Exception e) {
			setErreur(CHAMP_PRENOM, e.getMessage());
		}
		utilisateur.setPrenom(prenom);
		
		int s = 0;
		try {
			s = validationSolde(solde);
		} catch (Exception e) {
			setErreur(CHAMP_SOLDE, e.getMessage());
		}
		utilisateur.setSolde(s);
		
		MysqlGestionUtilisateur db = new MysqlGestionUtilisateur();
		if(db.existUtilisateur(utilisateur))
			setErreur(EXIST_USER, "Ce pseudo exist déja ");
		
		if (erreurs.isEmpty()) {
			resultat = "Succès de l'inscription.";
			
			db.inscrireUtilisateur(utilisateur);
			
		} else {
			resultat = "Échec de l'inscription.";
		}
		
		return utilisateur;
	}

	private void validationPseudo(String pseudo) throws Exception {
		if (pseudo != null) {
			if (pseudo.length() < 3) {
				throw new Exception("Merci de saisir un pseudo valide (5 caractères minimum).");
			}
		} else {
			throw new Exception("Merci de saisir un pseudo.");
		}
	}

	private void validationMotsDePasse(String motDePasse)
			throws Exception {
		if (motDePasse != null) {
			if (motDePasse.length() < 3) {
				throw new Exception(
						"Les mots de passe doivent contenir au moins 3 caractères.");
			}
		} else {
			throw new Exception(
					"Merci de saisir  un mot de passe.");
		}
	}

	private void validationNom(String nom) throws Exception {
		if (nom != null && nom.length() < 2) {
			throw new Exception(
					"Le nom d'utilisateur doit contenir au moins 2 caractères.");
		}
	}

	private void validationPrenom(String prenom) throws Exception {
		if (prenom != null && prenom.length() < 2) {
			throw new Exception(
					"Le prenom d'utilisateur doit contenir au moins 2 caractères.");
		}
	}
	
	private int validationSolde(String solde) throws Exception {
		int ret = 0;
		if (solde != null ) {
			try{
				int s = Integer.parseInt(solde);
				ret = s;
				if(s<0){
					throw new Exception(
							"Veuillez entrz un montant positif ou null.");
				}
			}catch(NumberFormatException e){
				throw new Exception(
						"Veuillez entrz un montant correct.");
			}
			
		}
		return ret;
	}

	
	/*
	 * Ajoute un message correspondant au champ spécifié à la map des erreurs.
	 */
	private void setErreur(String champ, String message) {
		erreurs.put(champ, message);
	}

	/*
	 * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
	 * sinon.
	 */
	private static String getValeurChamp(HttpServletRequest request,
			String nomChamp) {
		String valeur = request.getParameter(nomChamp);
		if (valeur == null || valeur.trim().length() == 0) {
			return null;
		} else {
			return valeur.trim();
		}
	}
}
