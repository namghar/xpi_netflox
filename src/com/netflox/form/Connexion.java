package com.netflox.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.netflox.dao.MysqlGestionUtilisateur;
import com.netflox.model.Utilisateur;

public class Connexion {
	private static final String CHAMP_PSEUDO = "pseudo";
	private static final String CHAMP_PASS = "motdepasse";
	private static final String EXIST_USER = "userexist";
	
	private String resultat;
	private Map<String, String> erreurs = new HashMap<String, String>();

	public String getResultat() {
		return resultat;
	}

	public Map<String, String> getErreurs() {
		return erreurs;
	}

	public Utilisateur connectionUtilisateur(HttpServletRequest request) {
		String pseudo = getValeurChamp(request, CHAMP_PSEUDO);
		String motDePasse = getValeurChamp(request, CHAMP_PASS);
		
		Utilisateur utilisateur = new Utilisateur();

		try {
			validationPseudo(pseudo);
		} catch (Exception e) {
			setErreur(CHAMP_PSEUDO, e.getMessage());
		}
		utilisateur.setPseudo(pseudo);

		try {
			validationMotsDePasse(motDePasse);
		} catch (Exception e) {
			setErreur(CHAMP_PASS, e.getMessage());			
		}		
		utilisateur.setMotDePasse(motDePasse);
		
		MysqlGestionUtilisateur db = new MysqlGestionUtilisateur();
		if(!db.canConnectUtilisateur(utilisateur))
			setErreur(EXIST_USER, "Pseudo ou mot de passe incorrect ");
		
		if (erreurs.isEmpty()) {
			resultat = "Succès de la connection.";
		} else {
			resultat = "Échec de la connection.";
		}

		return utilisateur;
	}

	private void validationPseudo(String pseudo) throws Exception {
		if (pseudo != null) {
			if (pseudo.length() < 3) {
				throw new Exception("Merci de saisir un pseudo valide ");
			}
		} else {
			throw new Exception("Merci de saisir un pseudo.");
		}
	}

	private void validationMotsDePasse(String motDePasse)
			throws Exception {
		if(motDePasse != null){
		 if (motDePasse.length() < 3) {
				throw new Exception("Mot de passe invalide");
			}
		}
		 else {
			throw new Exception(
					"Merci de saisir votre mot de passe.");
		}
	}

	/*
	 * Ajoute un message correspondant au champ spécifié à la map des erreurs.
	 */
	private void setErreur(String champ, String message) {
		erreurs.put(champ, message);
	}

	/*
	 * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
	 * sinon.
	 */
	private static String getValeurChamp(HttpServletRequest request,
			String nomChamp) {
		String valeur = request.getParameter(nomChamp);
		if (valeur == null || valeur.trim().length() == 0) {
			return null;
		} else {
			return valeur.trim();
		}
	}
}

