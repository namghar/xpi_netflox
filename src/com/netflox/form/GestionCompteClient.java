package com.netflox.form;

import java.util.ArrayList;

import com.netflox.dao.MysqlGestionUtilisateur;
import com.netflox.dao.MysqlGestionVente;
import com.netflox.model.Film;
import com.netflox.model.Utilisateur;
import com.netflox.model.Streaming;
import com.netflox.model.Vente;

public class GestionCompteClient {

	private Utilisateur user;
	private long date;
	
	public GestionCompteClient(Utilisateur u){
		user = u;
		date = System.currentTimeMillis();
	}
	
	private void miseAjourDate(){
		date = System.currentTimeMillis();
	}
	
	public ArrayList<Streaming> filmLoue(){
		ArrayList<Streaming> l = new ArrayList<Streaming>();
		MysqlGestionVente dbVente = new MysqlGestionVente();
		ArrayList<String[]> locations = dbVente.filmLoueUtilisateur(user);
		miseAjourDate();
		for(String[] loc : locations){
			//loc = {idFilm, date}
			long dateLoc = Long.parseLong(loc[1]);
			//48*60*60*1000 : 48h en ms
			if((dateLoc + (long)(48*60*60*1000)) > date){
				Streaming s = new Streaming(new Film(loc[0]), dateLoc);
				l.add(s);
			}
		}
		return l;
	}
	
	public ArrayList<Vente> histoAchat(){
		MysqlGestionVente dbVente = new MysqlGestionVente();
		return dbVente.venteUtilisateur(user);
	}
	
	public int rechargerCompte(int montant){
		MysqlGestionUtilisateur dbUti = new MysqlGestionUtilisateur();
		dbUti.updateSolde(user, montant);
		return dbUti.getSoldeUtilisateur(user);
	}
	
}
