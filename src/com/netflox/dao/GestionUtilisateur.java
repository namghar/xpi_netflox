package com.netflox.dao;

import java.util.ArrayList;

import com.netflox.model.Utilisateur;

public interface GestionUtilisateur {
	
	public void inscrireUtilisateur(Utilisateur utilisateur);
	public boolean existUtilisateur(Utilisateur utilisateur);
	public boolean canConnectUtilisateur(Utilisateur utilisateur);
	public void modifierUtilisateur(Utilisateur utilisateur);
	public ArrayList<Utilisateur> getAllUsers();
	public int getSoldeUtilisateur(Utilisateur utilisateur);
	public void updateSolde(Utilisateur utilisateur, int somme);
	public void deleteListUsers(ArrayList<Utilisateur> list);
}
