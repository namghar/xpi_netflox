package com.netflox.dao;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.netflox.model.Utilisateur;

public class MysqlGestionUtilisateur implements GestionUtilisateur {

	private Connection getConnection() {
		/* Connexion à la base de données */
		String url = "jdbc:mysql://localhost:3306/netflox";
		String utilisateur = "root";
		String motDePasse = "";
		Connection connexion = null;
		/* Chargement du driver JDBC pour MySQL */
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			/* Gérer les éventuelles erreurs ici. */
		}
		try {
			connexion = (Connection) DriverManager.getConnection(url,
					utilisateur, motDePasse);

		} catch (SQLException e) {
			/* Gérer les éventuelles erreurs ici */
		}
		return connexion;
	}

	private void closeConnection(Connection connexion) {
		if (connexion != null)
			try {
				/* Fermeture de la connexion */
				connexion.close();
			} catch (SQLException ignore) {
				/*
				 * Si une erreur survient lors de la fermeture, il suffit de
				 * l'ignorer.
				 */
			}
	}

	public void inscrireUtilisateur(Utilisateur utilisateur) {
		Connection connex;
		connex = getConnection();
		String insert = "INSERT INTO `user`(`id`, `pseudo`, `nom`, `prenom`, `solde`, `psswd`) VALUES ( ?, ?, ?, ?, ?, ?) ";

		try {
			// inscription
			PreparedStatement preStat = (PreparedStatement) connex
					.prepareStatement(insert);

			preStat.setInt(1, 0);
			preStat.setString(2, utilisateur.getPseudo());
			preStat.setString(3, utilisateur.getNom());
			preStat.setString(4, utilisateur.getPrenom());
			preStat.setInt(5, utilisateur.getSolde());
			preStat.setString(6, utilisateur.getMotDePasse());

			int id = -1;
			preStat.executeUpdate(preStat.asSql(),
					preStat.RETURN_GENERATED_KEYS);
			// Les id auto-générées sont retournées sous forme de ResultSet
			ResultSet ids = preStat.getGeneratedKeys();
			if (ids.next()) {
				id = (int) ((long) ids.getObject(1));
			}

			utilisateur.setId(id);

		} catch (SQLException ignore) {

		}
		closeConnection(connex);
	}

	@Override
	public boolean existUtilisateur(Utilisateur utilisateur) {
		Connection connex;
		boolean exist = false;
		connex = getConnection();
		String select = "SELECT * FROM user WHERE pseudo = ? ";
		try {
			PreparedStatement preStat = (PreparedStatement) connex
					.prepareStatement(select);

			preStat.setString(1, utilisateur.getPseudo());

			ResultSet rs = preStat.executeQuery();
			exist = rs.next();

		} catch (SQLException ignore) {

		}
		closeConnection(connex);
		return exist;
	}

	@Override
	public void modifierUtilisateur(Utilisateur utilisateur) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canConnectUtilisateur(Utilisateur utilisateur) {
		Connection connex;
		boolean exist = false;
		connex = getConnection();
		String select = "SELECT * FROM user WHERE pseudo = ? AND psswd = ?";
		try {
			PreparedStatement preStat = (PreparedStatement) connex
					.prepareStatement(select);

			preStat.setString(1, utilisateur.getPseudo());
			preStat.setString(2, utilisateur.getMotDePasse());

			ResultSet rs = preStat.executeQuery();
			exist = rs.next();
			//System.out.println(exist);
			if(exist){
				int id = rs.getInt(1);
				utilisateur.setId(id);
				utilisateur.setNom(rs.getString(3));
				utilisateur.setPrenom(rs.getString(4));
				utilisateur.setSolde(rs.getInt(5));
			}

		} catch (SQLException ignore) {

		}
		closeConnection(connex);
		return exist;
	}

	@Override
	public int getSoldeUtilisateur(Utilisateur utilisateur) {
		Connection connex;
		int solde = -1;
		connex = getConnection();
		String select = "SELECT solde FROM user WHERE pseudo = ?";
		try {
			PreparedStatement preStat = (PreparedStatement) connex
					.prepareStatement(select);

			preStat.setString(1, utilisateur.getPseudo());
			ResultSet rs = preStat.executeQuery();

			while(rs.next()){
				solde = rs.getInt(1);
			}
		} catch (SQLException ignore) {			
			System.out.println("getSoldeUtilisateur : ignore");
		}
		closeConnection(connex);
		return solde;
	}

	@Override
	public void updateSolde(Utilisateur utilisateur, int somme) {
		Connection connex;
		connex = getConnection();
		String select = "UPDATE user SET solde = solde + ? WHERE pseudo = ?";
		try {
			PreparedStatement preStat = (PreparedStatement) connex
					.prepareStatement(select);
			preStat.setInt(1, somme);
			preStat.setString(2, utilisateur.getPseudo());
			preStat.executeUpdate();
		} catch (SQLException ignore) {
			System.out.println("erreur updateSolde : " + ignore);
		}
		closeConnection(connex);
	}

	@Override
	public ArrayList<Utilisateur> getAllUsers() {
		ArrayList<Utilisateur> l = new ArrayList<Utilisateur>();
		Connection connex;
		connex = getConnection();
		String select = "SELECT  * FROM `user`";
		try {
			PreparedStatement preStat = (PreparedStatement) connex
					.prepareStatement(select);

			ResultSet rs = preStat.executeQuery();
			while (rs.next()) {
				Utilisateur u = new Utilisateur();
				u.setId(rs.getInt(1));
				u.setPseudo(rs.getString(2));
				u.setNom(rs.getString(3));
				u.setPrenom(rs.getString(4));
				u.setSolde(rs.getInt(5));
				u.setMotDePasse(rs.getString(6));
				l.add(u);
			}
		} catch (SQLException ignore) {
			System.out.println("erreur tous utilisateur: " + ignore);
		}
		closeConnection(connex);
		return l;
	}

	@Override
	public void deleteListUsers(ArrayList<Utilisateur> list) {

		Connection connex;
		connex = getConnection();
		String delete = "DELETE FROM `user` WHERE id = ?";
		try {
			PreparedStatement preStat = (PreparedStatement) connex
					.prepareStatement(delete);
			
		for (Utilisateur u : list) {
			preStat.setInt(1, u.getId());			
			preStat.executeUpdate();	
		}

		
		} catch (SQLException ignore) {
			System.out.println("erreur tous utilisateur: " + ignore);
		}
		closeConnection(connex);
		
	}
}
