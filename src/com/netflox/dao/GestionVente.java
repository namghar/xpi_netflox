package com.netflox.dao;

import java.util.ArrayList;

import com.netflox.model.*;

public interface GestionVente {
	
	public int ajouterVente(Vente v);
	public void ajouterBon(Bon b);
	public ArrayList<Vente> venteUtilisateur(Utilisateur c);
	public ArrayList<String[]> filmLoueUtilisateur(Utilisateur c);
	public ArrayList<String> filmsInfosVenteLoueBon(String type);
	public int bonValide(int id);
	public void updateBon(Bon b);
}