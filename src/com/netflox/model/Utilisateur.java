package com.netflox.model;

public class Utilisateur {


	private int id;
	private String pseudo;
	private String motDePasse;
	private String nom;
	private String prenom;
	private int solde = 0;	


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setSolde(int solde) {
		this.solde = solde;
	}

	public int getSolde() {
		return solde;
	}
}
