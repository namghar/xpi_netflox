package com.netflox.model;


public class Bon {

	private int id; //correspond id vente
	private String idFilm; //Si utilise
	private long dateU; //date d'utilisation
	private String type; //l ou a
	private int idClientU; //id client utilisateur
	private int utilise;
	private String codeBon;
	private int montant;
	
	public Bon(){
		this.id = 0;
		this.montant = 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIdFilm() {
		return idFilm;
	}

	public void setIdFilm(String idFilm) {
		this.idFilm = idFilm;
	}

	public long getDateU() {
		return dateU;
	}

	public void setDateU(long dateU) {
		this.dateU = dateU;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getIdClientU() {
		return idClientU;
	}

	public void setIdClientU(int idClientU) {
		this.idClientU = idClientU;
	}

	public int getUtilise() {
		return utilise;
	}

	public void setUtilise(int utilise) {
		this.utilise = utilise;
	}

	public String getCodeBon() {
		return codeBon;
	}

	public int getMontant() {
		return montant;
	}

	public void setMontant(int montant) {
		this.montant = montant;
	}
	
	public void genererCode(){
		this.codeBon = this. montant + "NetFLOX##" + this.id;
	}
}
