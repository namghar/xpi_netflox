package com.netflox.model;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DownloadHelper {
	
	public void downloadLink(HttpServletRequest request, HttpServletResponse response,String filePath,String fileName) throws IOException{
		response.setContentType("application/download");
		response.setHeader("Content-waDisposition", "attachment;filename=\"" + fileName + "\"");
		
		ServletOutputStream out = response.getOutputStream();
		File file = null;
		BufferedInputStream from = null;
		
		try { 
			file = new File(filePath+File.separator+fileName);
			response.setContentLength((int) file.length()); 
			int bufferSize = 64 * 1024;
		
			from = new BufferedInputStream(new FileInputStream(file), bufferSize * 2);
			byte[] bufferFile = new byte[bufferSize];
			
			for (; ;) {
				
				int len = from.read(bufferFile); 
				
				if (len < 0) {
					break;
				} 
				
				out.write(bufferFile, 0, len);
			}
			out.flush();
				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (from != null) {
				try {
					from.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (out != null) {
				try {
					out.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (file != null) {
				try {
					file.delete();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
