package com.netflox.model;
public class Acteur {

	private String nom = "";
	private String prenom = "";
	private String role = "";

	public Acteur() {

	}

	public Acteur(String nom, String prenom, String role) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.role = role;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}