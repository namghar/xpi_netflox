package com.netflox.model;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;

import com.netflox.pdf.pdfCreator;




public class FilmsManager {

	/**
	 * @param args
	 */

	private int noOfFilms = 0;
	private String MOVIES;

	public FilmsManager(String inP) {
		MOVIES = inP + File.separator + "movies.xml";
	}

	public List<Film> parse() {
		File _FilePath = new File(MOVIES);
		org.jdom.Document document = null;
		List<Film> l = new ArrayList<Film>();

		try {
			/* On cree une instance de SAXBuilder */
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(_FilePath);
		} catch (IOException e) {
			System.out.println("Erreur lors de la lecture du fichier "
					+ e.getMessage());
			e.printStackTrace();
		} catch (JDOMException e) {
			System.out
					.println("Erreur lors de la construction du fichier JDOM "
							+ e.getMessage());
			e.printStackTrace();
		}

		try {
			
			Element racine = document.getRootElement();
			/* Recherche de la liste des Films */
			XPath xpa = XPath.newInstance("//movie");

			l.addAll(getFilmsInfo(xpa, racine));

//			Sauvegarder la taille de la liste des films
			noOfFilms = l.size();
		} catch (JDOMException e) {
			System.out.println("Erreur JDOM " + e.getMessage());
			e.printStackTrace();
		}
		return l;
	}

	public List<Film> sortPerTitle(List<Film> films) {
//		Trier la liste des films par ordre alphabetique
		Collections.sort(films, Film.Comparators.TITRE);
		return films;
	}

	public List<Film> sortPerYear(List<Film> films) {
//		Trier la liste des films par ordre chronologique
		Collections.sort(films, Film.Comparators.ANNEE);
		return films;
	}

	public int getNoOfFilms() {
		return noOfFilms;
	}
	
//	Retourner une sous partie de la liste des films pour permettre un affichage ergonomique d'un resultat 
	public List<Film> FilmsPerPage(List<Film> films, int range,
			int noFilmsPerPage, String sort) {
		List<Film> l = new ArrayList<Film>();
//		Trie par ordre alphabetique
		if (sort.equalsIgnoreCase("T"))
			l.addAll(sortPerTitle(films));
//		Trie par ordre chronologique
		else if (sort.equalsIgnoreCase("Y"))
			l.addAll(sortPerYear(films));
		else
			System.err.println("Error");
		List<Film> res = new ArrayList<Film>();
		for (int i = range * noFilmsPerPage; i < (range + 1) * noFilmsPerPage; i++) {
			if (i < l.size())
				res.add(l.get(i));
		}

		return res;
	}
	
//	Retourner une sous partie de la liste des films pour permettre un affichage ergonomique du catalogue
	public List<Film> FilmsPerPage(int range,
			int noFilmsPerPage, String sort) {
		List<Film> films = this.parse();
		List<Film> l = new ArrayList<Film>();
//		Trie par ordre alphabetique
		if (sort.equalsIgnoreCase("T"))
			l.addAll(sortPerTitle(films));
//		Trie par ordre chronologique
		else if (sort.equalsIgnoreCase("Y"))
			l.addAll(sortPerYear(films));
		else
			System.err.println("Error");
		List<Film> res = new ArrayList<Film>();
		for (int i = range * noFilmsPerPage; i < (range + 1) * noFilmsPerPage; i++) {
			if (i < l.size())
				res.add(l.get(i));
		}

		return res;
	}

	public List<Film> searchResult(String searchElement, String categorie) {
		File _FilePath = new File(MOVIES);
		org.jdom.Document document = null;
		List<Film> l = new ArrayList<Film>();
		try {
			/* On cree @une instance de SAXBuilder */
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(_FilePath);
		} catch (IOException e) {
			System.out.println("Erreur lors de la lecture du fichier "
					+ e.getMessage());
			e.printStackTrace();
		} catch (JDOMException e) {
			System.out
					.println("Erreur lors de la construction du fichier JDOM "
							+ e.getMessage());
			e.printStackTrace();
		}

		try {

			Element racine = document.getRootElement();
			XPath xpa = null;
//			Trouver l'element rechercher suivant sa categorie en utilisant du XPATH
			if (categorie.equalsIgnoreCase("Description")) {
				xpa = XPath.newInstance("//movie[ contains( summary, \""
						+ searchElement + "\")]");
			} else if (categorie.equalsIgnoreCase("Titre")){
				xpa = XPath.newInstance("//movie[ contains( title, \""
						+ searchElement + "\")]");
			}
			
			else if (categorie.equalsIgnoreCase("Acteur"))
				xpa = XPath
						.newInstance("//movie[ contains( actor/name/firstname, \""
								+ searchElement
								+ "\") or contains( actor/name/lastname, \""
								+ searchElement + "\")]"); // Que les premiers
			l.addAll(getFilmsInfo(xpa, racine));
		} catch (JDOMException e) {
			System.out.println("Erreur JDOM " + e.getMessage());
			e.printStackTrace();
		}
		return l;
	}
	public void  ajouterFilm(String id,	
			String title,
			String year,
			String runtime,
			String description,
			List<Realisateur> realisateurs,
			List<Acteur> acteurs,
			List<AlternateTitle> alternateTitles){
//		Le parse du fichier XML
		File _FilePath = new File(MOVIES);
		org.jdom.Document document = null;

		try {
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(_FilePath);
		} catch (IOException e) {
			System.out.println("Erreur lors de la lecture du fichier "
					+ e.getMessage());
			e.printStackTrace();
		} catch (JDOMException e) {
			System.out
			.println("Erreur lors de la construction du fichier JDOM "
					+ e.getMessage());
			e.printStackTrace();
		}
		Element racine = document.getRootElement();
//		Construction d'un noeud contenant les nouveaux informations du film
		Element child = new Element("movie").setAttribute("id",id);

		child.addContent(new Element("title").setText(title))
		.addContent(new Element("year").setText(year))
		.addContent(new Element("runtime").setText(runtime));
		// Ajouter Realisateur
		Element noeudRealisateurs = new Element("directors");
		for(int i = 0; i<realisateurs.size(); i++ ){
		Element noeudRealisateur = new Element("director");
		noeudRealisateur.addContent(new Element("name")
								.addContent(new Element("firstname").setText(acteurs.get(i).getPrenom()))
								.addContent(new Element("lastname").setText(acteurs.get(i).getNom())));

								noeudRealisateurs.addContent(noeudRealisateur);
		}
		child.addContent(noeudRealisateurs);
		// Ajouter Acteur
		for(int i = 0; i<acteurs.size(); i++ ){
		Element noeudActeurs = new Element("actor");
		noeudActeurs.addContent(new Element("name")
								.addContent(new Element("firstname").setText(acteurs.get(i).getPrenom()))
								.addContent(new Element("lastname").setText(acteurs.get(i).getNom())))
				.addContent(new Element("role").setText(acteurs.get(i).getRole()));

		child.addContent(noeudActeurs);
		}
		//Ajouter Description
		child.addContent(new Element("summary").setText(description));
		
		// Ajouter AlternateTitle
		Element noeudAlteranteTitles = new Element("alternate_titles");
		for(int i = 0; i<alternateTitles.size(); i++ ){
			Element noeudAlteranteTitle = new Element("atitle").setAttribute("country",alternateTitles.get(i).getId());
			noeudAlteranteTitle.setText(alternateTitles.get(i).getTitre());
			noeudAlteranteTitles.addContent(noeudAlteranteTitle);
		}		
		child.addContent(noeudAlteranteTitles);
		racine.addContent(child);
		XMLOutputter outputter = 
				new XMLOutputter(Format.getPrettyFormat());
		try {
//			Creation du nouveau fichier XML qui contient aussi le nouveau film
			outputter.output(document,new PrintWriter(_FilePath,"UTF-8"));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	//	Recupere les infos du film
	public List<Film> getFilmsInfo(XPath xpa, Element racine) {
		List results;
		List<Film> l = new ArrayList<Film>();
		try {
			results = xpa.selectNodes(racine);
			Iterator iter = results.iterator();
			Film f = null;
			Element noeudCourant = null;
			Element noeudCourantActeur = null;
			while (iter.hasNext()) { // iter sur le 1 er elements ***
				f = new Film();
//				  Pour chaque film nous allons chercher ses inforrmations
				noeudCourant = (Element) iter.next();
//				Recuperer l'id le titre et l'année 
				xpa = XPath.newInstance("./@id");
				f.setId(xpa.valueOf(noeudCourant));
				
				xpa = XPath.newInstance("./title");
				f.setTitre(xpa.valueOf(noeudCourant));

				xpa = XPath.newInstance("./year");
				f.setAnnee(xpa.valueOf(noeudCourant));
				
				xpa = XPath.newInstance("./summary");
				f.setDescription(xpa.valueOf(noeudCourant));
				
//				Recuperer le nom, prenom et le role d'un acteur 
				XPath xpaAct = XPath.newInstance("./actor");

				List resultsAct = xpaAct.selectNodes(noeudCourant);
				Iterator iterAct = resultsAct.iterator();
				while (iterAct.hasNext()) {
					Acteur act = new Acteur();
					noeudCourantActeur = (Element) iterAct.next();
					xpaAct = XPath.newInstance("./name/firstname");
					act.setPrenom(xpaAct.valueOf(noeudCourantActeur));

					xpaAct = XPath.newInstance("./name/lastname");
					act.setNom(xpaAct.valueOf(noeudCourantActeur));

					xpaAct = XPath.newInstance("./role");
					act.setRole(xpaAct.valueOf(noeudCourantActeur));
					ArrayList<Acteur> la = f.getTabActeur();
					la.add(act);
					f.setTabActeur(la);
				}

				l.add(f);
			}
			sortPerTitle(l);
			noOfFilms = l.size();
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return l;

	}
	
	public String getId(){
		int  i = 0;
		List<Film> l = this.parse();
//		Recupere le max des id films dans une TreeSet
		TreeSet ts = new TreeSet();
		System.out.println(MOVIES);

		for(Film f : l){
			ts.add(Integer.parseInt(f.getId().substring(2)));
		}
		i = (int) ts.last();
//		Incremente et recree un nouveau id
		i++;
		String s = "tt" + i;
		return s;
	}
	
	public Film getFilm(String id){
		List<Film> l = parse();
		for(Film f: l){
			if(f.getId().equals(id)){
				return f;
			}
		}
		return null;
			
	}
}
