package com.netflox.model;


public class Vente {

		private int id;
		private String idFilm;
		private long date;
		private int prix;
		private int idClient;
		private String type;	//a ou l ou b
		
		public Vente(){
			
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getIdFilm() {
			return idFilm;
		}

		public void setIdFilm(String idFilm) {
			this.idFilm = idFilm;
		}
		
		public long getDate() {
			return date;
		}

		public void setDate(long date) {
			this.date = date;
		}

		public int getPrix() {
			return prix;
		}

		public void setPrix(int prix) {
			this.prix = prix;
		}

		public int getIdClient() {
			return idClient;
		}

		public void setIdClient(int idClient) {
			this.idClient = idClient;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}	
}
