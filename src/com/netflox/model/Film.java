package com.netflox.model;

import java.util.ArrayList;
import java.util.Comparator;

public class Film implements Comparable<Film> {
	
	private String id;
	private String titre = "";
	private String annee = "";
	private String description = "";
	private ArrayList<Acteur> tabActeur = new ArrayList<Acteur>();
	
	public Film() {

	}
	
	public Film(String ident){
		id = ident;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<Acteur> getTabActeur() {
		return tabActeur;
	}

	public void setTabActeur(ArrayList<Acteur> tabActeur) {
		this.tabActeur = tabActeur;
	}

	public String getTitre() {
		return titre;
	}

	public String getAnnee() {
		return annee;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public void setAnnee(String annee) {
		this.annee = annee;
	}

	@Override
	public int compareTo(Film arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static class Comparators {

		public static Comparator<Film> TITRE = new Comparator<Film>() {
			@Override
			public int compare(Film arg0, Film arg1) {
				return arg0.titre.compareToIgnoreCase(arg1.titre);
			}
		};
		public static Comparator<Film> ANNEE = new Comparator<Film>() {
			@Override
			public int compare(Film o1, Film o2) {
				int v1 = 0;
				int v2 = 0;
				try {
					v1 = Integer.parseInt(o1.annee);					
				} catch (NumberFormatException e) {
					v1 = Integer.MAX_VALUE;
				}
				try {					
					v2 = Integer.parseInt(o2.annee);
				} catch (NumberFormatException e) {
					v2 = Integer.MAX_VALUE;
				}
				
				return v1 - v2;

			}
		};

	}
}
