package com.netflox.model;

import java.util.Date;

public class Streaming {
	
	private Film film;
	private long dateDebut;
	private long dateFin;
	private final long dureeStreaming = 48 * 60 * 60 * 1000;
	
	public Streaming(Film f, long dD){
		film = f;
		dateDebut = dD;
		dateFin = dD + dureeStreaming;
	}
	
	public long tempsRestantH(){
		long res = dateFin - System.currentTimeMillis();
		long totalSeconds = res/1000;  
	    long second = (int)(totalSeconds%60);  
	    long totalMinutes = totalSeconds/60;
	    long totalHours = totalMinutes/60;  
		return (int)(totalHours%24);
	}
	
	public long tempsRestantM(){
		long res = dateFin - System.currentTimeMillis();
		long totalSeconds = res/1000;  
	    long second = (int)(totalSeconds%60);  
	    long totalMinutes = totalSeconds/60;  
		return totalMinutes%60;
	}

	public Film getFilm() {
		return film;
	}

	public void setFilm(Film film) {
		this.film = film;
	}

	public long getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(long dateDebut) {
		this.dateDebut = dateDebut;
	}

	public long getDateFin() {
		return dateFin;
	}

	public void setDateFin(long dateFin) {
		this.dateFin = dateFin;
	}

	public long getDureeStreaming() {
		return dureeStreaming;
	}
	

}
