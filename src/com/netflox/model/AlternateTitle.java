package com.netflox.model;

public class AlternateTitle {
	private String id = "";
	private String titre = "";
	
	public AlternateTitle(String id, String titre) {
		super();
		this.id = id;
		this.titre = titre;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}


}
